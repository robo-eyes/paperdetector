pylint-gitlab==1.2.0
pytest==7.2.0
pytest-cov==4.0.0
pytest-html==3.2.0
pytest-timeout==2.1.0
autopep8==2.0.0
