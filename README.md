[![pylint](https://robo-eyes.gitlab.io/paperdetector/badges/pylint.svg)](https://robo-eyes.gitlab.io/paperdetector/lint/)

# PaperDetector

Detector for paper


## Dataset
Dataset consists of data from
- [A New Image Dataset for Document Corner Localization](https://data.mendeley.com/datasets/x3nm4cxr83/2#folder-ea863ca1bb80-407b-91ac-aabea6a33ede).

## Model
Rebuilds the model(s) from [Real-time Document Localization in Natural Images by Recursive Application of a CNN](https://khurramjaved.com/RecursiveCNN.pdf) in Keras.

## Training
You can train the model(s) by running `python src/train.py corner_detection corner_refiner`.
You can use the provided Docker Container for training, it has all the dependencies installed.
After training you can show the Tensorboard with `tensorboard --logdir logs/fit/[model]`


## Resources
- [A New Image Dataset for Document Corner Localization](https://ieeexplore.ieee.org/document/9116896)
- [Real-time Document Localization in Natural Images by Recursive Application of a CNN](https://khurramjaved.com/RecursiveCNN.pdf)


## Known Issues
>1) Lack of precise features: Standard Deep Convolutional
Neural Network architectures are inherently bad at precise
localization and segmentation tasks [19]. This is because the
last convolutional layer only contains high-level features of
the whole image. While these features are extremely useful
for classification and bounding box detection, they lack the
information for pixel level segmentation. Several complicated
architectures have been proposed that are better at precise spatial predictions [20], but these architectures are often difficult
to implement and train. Furthermore models that only use high
level features of the whole image can be argued to have better
generalization.
>2) Upscale error: Upscaling a prediction made on 32 × 32
image introduces a large error. For instance for an original
image of 640 × 640 pixels, the upscaling can introduce an
error of ± 10 pixels.

[source](https://khurramjaved.com/RecursiveCNN.pdf)

## Development Container

The repo holds a Dockerfile, that can be used as development and training environment.
