FROM tensorflow/tensorflow:2.9.1-gpu-jupyter

ARG USERNAME=paperdetector
ARG USER_UID=1000
ARG USER_GID=$USER_UID
ENV SHELL /bin/bash

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# ********************************************************
# * Anything else you want to do like clean up goes here *
# ********************************************************



COPY ./src src
COPY ./requirements requirements

RUN pip install --upgrade pip
RUN pip install -r requirements/requirements.txt
RUN pip install -r requirements/doc_requirements.txt
RUN pip install -r requirements/test_requirements.txt
# RUN apt install git -y
RUN apt update
RUN apt install openssh-client -y
## dependencies needed for OpenCV in Container
RUN apt install ffmpeg libsm6 libxext6 libgl1 -y
# Adding installed scripts to PATH
RUN export PATH="/home/$USERNAME/.local/bin:$PATH" 
CMD python src/train.py


# [Optional] Set the default user. Omit if you want to keep the default as root.
USER $USERNAME
