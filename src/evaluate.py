'''
Evaluates the model(s)
'''
# System imports
import argparse
from pathlib import Path
from collections import defaultdict

# 3rd party imports
from tensorflow.keras.models import load_model
import numpy as np
from paz.models.detection.paper_detection import PaperDetection, CornerRefiner
from paz.pipelines.detection import DetectPaper
from paz.backend.keypoints import normalize_keypoints2D
import tensorflow as tf


# local imports
from paperdetector.data import *
from paperdetector.utils.eval_utils import *
from train import AVAILABLE_MODELS

# end file header


__author__ = 'Adrian Lubitz'


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Evaluates the model in a given path and saves statistics to a file.')
    # TODO: add more arguments like shape and version
    parser.add_argument('-d', '--detection_model', default=None,
                        help='Path to the detection model. If no model is given the model from paz.models.detection.paper_detection.PaperDetection is used.')
    parser.add_argument('-r', '--refiner_model', default=None,
                        help='Path to the refiner model. If no model is given the model from paz.models.detection.paper_detection.CornerRefiner is used.')
    parser.add_argument('-o', '--output', default='evaluations',
                        help='Folder to hold the evaluation files. If none is given an evaluations directory will be created(if not exsit) in the current working directory')
    # TODO: this may be removed in the future because I always want to evaluate everything inlcuding the combined pipeline
    parser.add_argument('-t', '--type', choices=AVAILABLE_MODELS,
                        help='Type of the model to evaluate. If not given we try to evaluate from the given model')
    args = parser.parse_args()

    if args.detection_model:
        raise NotImplementedError(
            'this is not implemented right now - only taking models from paz')
    else:
        detection_model = PaperDetection()
        detection_model_path = Path('paz')

    if args.refiner_model:
        raise NotImplementedError(
            'this is not implemented right now - only taking models from paz')
    else:
        refiner_model = CornerRefiner()
        refiner_model_path = Path('paz')

    out_path = Path(
        args.output, f'{detection_model_path.name}_{refiner_model_path.name}')
    out_path.mkdir(parents=True, exist_ok=True)
    eval_path = Path(out_path, 'evaluate.txt')

    d = Dataset()
    train_samples, valid_samples, test_samples = d.make_split()


###### DETECTION TESTS ###########
    all_coordinates, all_predictions = make_predictions(
        test_samples, detection_model)
    jaccard_results = calc_jaccard(
        all_coordinates, all_predictions, test_samples)
    mae_results = calc_mae(all_coordinates, all_predictions, test_samples)

    detection_test_dataGen = DataGenerator(
        test_samples, data_type='corner_detection', shape=(32, 32))
    scores = detection_model.evaluate(detection_test_dataGen)


####### REFINER TESTS  ###############


##### PIPELINE TESTS ############
    paper_detection_pipeline = DetectPaper()
    all_optimized_predictions = []
    for sample_path in test_samples:
        # load image
        sample = Sample(sample_path)
        image = sample._get_image()
        image = np.array(image)
        # put image in pipline and save prediction in all_optimized_predictions
        # TODO: prediction needs to be translated to relative keypoints - can be done with normalize_keypoints2d from paz
        prediction = paper_detection_pipeline(image)
        rel_prediction = normalize_keypoints2D(
            prediction, sample.height, sample.width, norm_range=(0, 1))
        all_optimized_predictions.append(rel_prediction)

    # all_optimized_predictions are absolute values!!!
    all_optimized_predictions = np.array(all_optimized_predictions)
    all_optimized_predictions = all_optimized_predictions.reshape(-1, 8)
    # calculate jaccard and mae for all_optzimized_predictions
    optimized_jaccard_results = calc_jaccard(
        all_coordinates, all_optimized_predictions, test_samples)
    optimized_mae_results = calc_mae(
        all_coordinates, all_optimized_predictions, test_samples)


######### WRITE REPORT ##############

    with open(eval_path, 'w') as outfile:
        for i, metric in enumerate(detection_model.metrics_names):
            print(f'{metric}:{scores[i]}', file=outfile)
        print(
            f'mean_jaccard:{jaccard_results["mean"]}', file=outfile)
        print(
            f'np.mae:{mae_results["mean"]}', file=outfile)
        print(
            f'optimized_mean_jaccard:{optimized_jaccard_results["mean"]}', file=outfile)
        print(
            f'optimized_np.mae:{optimized_mae_results["mean"]}', file=outfile)
    jaccard_graphic(jaccard_results, out_path)
    mae_graphic(mae_results, out_path)
