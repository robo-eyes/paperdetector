'''
Training script for the models
'''

# System imports
import argparse
import datetime
from pathlib import Path


# 3rd party imports
import tensorflow as tf
import numpy as np
from paz.models.detection.paper_detection import build_model
from paz.processors import SequentialProcessor
from paz import processors as pr


# local imports
from paperdetector.data import Dataset, DataGenerator


# end file header
__author__ = 'Adrian Lubitz'

AVAILABLE_MODELS = ['corner_detection', 'corner_refiner']

# source: https://stackoverflow.com/questions/8624034/python-argparse-type-and-choice-restrictions-with-nargs-1


class ValidateModels(argparse.Action):
    def __call__(self, parser, args, values, option_string=None):
        for model in values:
            if model.lower() not in AVAILABLE_MODELS:
                raise ValueError(
                    f'model can only be a combination of {AVAILABLE_MODELS} but is {values}')
            setattr(args, self.dest, values)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    # TODO: add more arguments like augmentation
    parser.add_argument('-v', '--version',
                        help='A Version for the model', default='0.0.dev')
    parser.add_argument('models', nargs='+',
                        action=ValidateModels, help=f'list of models that should be trained. Can be any combination of {AVAILABLE_MODELS}')
    parser.add_argument('-e', '--epochs', default=20, type=int,
                        help='number of epochs to train')
    parser.add_argument('-a', '--enable_augmentation', default=False, action="store_true",
                        help='If flag is set augmentation is enabled.')
    parser.add_argument('-i', '--initial_value_threshold', default=0.15, type=float,
                        help='The model will only be saved if the validation metric is smaller than this value')
    args = parser.parse_args()

    # Load data
    d = Dataset()
    training, validation, test = d.make_split()
    augmentation_processor = SequentialProcessor([
        # We expect samples to be normalizes here
        pr.DenormalizeKeypoints2D(norm_range=(0, 1)),
        pr.RandomKeypointRotation(),
        pr.RandomKeypointTranslation(),
        pr.NormalizeKeypoints2D(norm_range=(0, 1))
    ]) if args.enable_augmentation else None

    print(f"Starting training with {args=}")

    for model_name in args.models:
        # DataGenerator
        train_dataGen = DataGenerator(
            training, data_type=model_name, shape=(32, 32), augemtation_processor=augmentation_processor)
        validation_dataGen = DataGenerator(
            validation, data_type=model_name, shape=(32, 32))
        # load models
        # Note: this can support other parameters as well
        model = build_model(model_name)
        current_time = datetime.datetime.now().strftime(
            "%Y%m%d-%H%M%S")  # TODO: set to local time?
        log_dir = Path(
            "logs", "fit", f"{model_name}.v{args.version}{current_time}")
        Path("models").mkdir(parents=True, exist_ok=True)
        modelpath = Path(
            "models", f'{model_name}.v{args.version}{current_time}')

        ### Callbacks ###
        tensorboard_callback = tf.keras.callbacks.TensorBoard(
            log_dir=log_dir, histogram_freq=1)
        checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
            filepath=modelpath,
            save_weights_only=False,
            monitor='val_mae',
            mode='min',
            save_best_only=True,
            save_freq='epoch',
            initial_value_threshold=args.initial_value_threshold)  # Only saves if the error is smaller than 15%
        history = model.fit(train_dataGen, validation_data=validation_dataGen,
                            epochs=args.epochs, callbacks=[tensorboard_callback, checkpoint_callback])

        # model.save(modelpath)
