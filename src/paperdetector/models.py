'''
This module loads existing models and defines new models
'''

# System imports
from pathlib import Path

# 3rd party imports
import tensorflow as tf
from tensorflow.keras import layers


# local imports
# from .utils import download_url, load_pb

# end file header
__author__ = 'Adrian Lubitz'


def build_model(model_name, **kwargs):
    """
    loads the model with the corresponding model name
    Args:
        model_name: Name of the model. Can be of 'corner_detection', 'corner_refiner'
    Returns:
        model: a keras model
    """
    if model_name.lower() == 'corner_detection':
        return build_khurramjaved96_get_corners(**kwargs)
    if model_name.lower() == 'corner_refiner':
        return build_khurramjaved96_refine_corner(**kwargs)


def build_khurramjaved96_get_corners():  # TODO:support arguments for shape and maybe others
    '''
    rebuild the corner detection model from https://khurramjaved.com/RecursiveCNN.pdf 
    '''
    # TODO: more modern approach is maybe to use seperabelConv2D: https://keras.io/examples/vision/keypoint_detection/
    model = tf.keras.Sequential()
    model.add(tf.keras.Input(shape=(32, 32, 3)))
    # Conv 1
    model.add(layers.Conv2D(kernel_size=(5, 5), activation='relu', filters=20,
              padding='same'))  # TODO: maybe use 3D filter instead of 2D?
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=(2, 2)))
    # Conv 2
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=40, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=40, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=(2, 2)))
    # Conv 3
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=60, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=60, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=(2, 2)))
    # Conv 4
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=80, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=(2, 2)))
    # Conv 5
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=100, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=(2, 2)))
    # Conv 6
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=100, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.Flatten())
    # Fully Connected 1
    # TODO: Paper stated 0.8 dropout rate
    model.add(layers.Dense(500, activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(500, activation='relu'))
    model.add(layers.Dropout(0.5))
    # Sigmoid Activation
    # regression is needed here -> sigmoid
    model.add(layers.Dense(8, activation='sigmoid'))
    model.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer='adam', metrics=[
                  'mse', 'mae'])  # metrics from https://keras.io/api/metrics/regression_metrics/
    model._name = 'corner_detection'
    return model


def build_khurramjaved96_refine_corner():  # TODO:support arguments for shape and maybe others
    '''
    rebuild the corner refiner model from https://khurramjaved.com/RecursiveCNN.pdf 
    '''
    # TODO: more modern approach is maybe to use seperabelConv2D: https://keras.io/examples/vision/keypoint_detection/
    model = tf.keras.Sequential()
    model.add(tf.keras.Input(shape=(32, 32, 3)))
    # Conv 1
    model.add(layers.Conv2D(kernel_size=(5, 5), activation='relu', filters=10,
              padding='same'))  # TODO: maybe use 3D filter instead of 2D?
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=(2, 2)))
    # Conv 2
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=10, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=(2, 2)))
    # Conv 3
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=20, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=(2, 2)))
    # Conv 4
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=30, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.MaxPool2D(pool_size=(2, 2)))
    # Conv 5
    model.add(layers.Conv2D(kernel_size=(5, 5),
              activation='relu', filters=40, padding='same'))
    model.add(layers.BatchNormalization())
    model.add(layers.Flatten())
    # Fully Connected 1
    # TODO: Paper stated 0.8 dropout rate
    model.add(layers.Dense(300, activation='relu'))
    model.add(layers.Dropout(0.5))
    # Sigmoid Activation
    # regression is needed here -> sigmoid
    model.add(layers.Dense(2, activation='sigmoid'))
    model.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer='adam', metrics=[
                  'mse', 'mae'])  # metrics from https://keras.io/api/metrics/regression_metrics/
    model._name = 'corner_refiner'
    return model


def load_khurramjaved96_get_corners(filename='getCorners', url='https://github.com/khurramjaved96/Recursive-CNNs/raw/server_branch/TrainedModel/getCorners.pb'):
    '''
    THIS IS NOT WORKING!
    loads the model from https://github.com/khurramjaved96/Recursive-CNNs/blob/server_branch/TrainedModel/getCorners.pb if not already on disk
    '''
    # if not Path(filename).is_file():
    #     download_url(url=url, output_path=filename)
    # model = load_pb(filename)
    # model = tf.keras.models.load_model('src/getCorners')

    # SOURCE: https://stackoverflow.com/questions/64945037/how-to-load-a-saved-tensorflow-model-and-evaluate-it

    raise NotImplementedError()

    loaded = tf.saved_model.load('src/getCorners')

    class LayerFromSavedModel(tf.keras.layers.Layer):
        def __init__(self):
            super(LayerFromSavedModel, self).__init__()
            self.vars = loaded.variables

        def call(self, inputs):
            return loaded.signatures['serving_default'](inputs)

    input = tf.keras.Input([32, 32])
    model = tf.keras.Model(input, LayerFromSavedModel()(input))

    return model
