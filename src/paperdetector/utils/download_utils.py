'''
This module provides helpful utils
'''

# System imports

# 3rd party imports
from tqdm import tqdm
import urllib.request
import tensorflow as tf
# import numpy as np
# from shapely.geometry import Polygon


# local imports
# from .models import *
# from .data import Sample

# end file header
__author__ = 'Adrian Lubitz'


def load_pb(path_to_pb):
    '''
    Loads Network Structure from a protobuf file
    '''
    with tf.compat.v1.gfile.GFile(path_to_pb, "rb") as f:
        graph_def = tf.GraphDef()  # TODO: what is the TF2 syntax here?
        graph_def.ParseFromString(f.read())
    with tf.Graph().as_default() as graph:
        tf.import_graph_def(graph_def, name='')
        return graph


class DownloadProgressBar(tqdm):
    """
    Download progressbar. Source: https://github.com/tqdm/tqdm#hooks-and-callbacks
    """

    def update_to(self, num_bytes=1, bsize=1, tsize=None):
        """
        update for progresbar
        """
        if tsize is not None:
            self.total = tsize
        self.update(num_bytes * bsize - self.n)


def download_url(url, output_path):
    """
    Download with progressbar. Source: https://github.com/tqdm/tqdm#hooks-and-callbacks
    """
    with DownloadProgressBar(unit='B', unit_scale=True,
                             miniters=1, desc=url.split('/')[-1]) as t_bar:
        urllib.request.urlretrieve(
            url, filename=output_path, reporthook=t_bar.update_to)
