'''
This module provides helpful utils
'''

# System imports
import argparse
from pathlib import Path
from collections import defaultdict

# 3rd party imports
from tqdm import tqdm
import urllib.request
import tensorflow as tf
import numpy as np
from shapely.geometry import Polygon
import matplotlib.pyplot as plt


# local imports
# from .models import *
from ..data import Sample

# end file header
__author__ = 'Adrian Lubitz'


def poly_area(x, y):
    """
    Calculates area of polygon. 
    Source https://stackoverflow.com/questions/24467972/calculate-area-of-polygon-given-x-y-coordinates
    """
    return 0.5*np.abs(np.dot(x, np.roll(y, 1))-np.dot(y, np.roll(x, 1)))


def _jaccard_index(first_polygon: list, second_polygon: list):
    """
    Calculate Jaccard Index for two polygons according to https://khurramjaved.com/RecursiveCNN.pdf
    Args:
        first_polygon: List of coordinates of a polygon
        second_polygon: List of coordinates of a second polygon
    Returns:
        float: Jaccard Index
    """
    # TODO: batch implementation with mean of all jaccard index results
    first_polygon = Polygon(first_polygon)
    second_polygon = Polygon(second_polygon)
    inter_area = first_polygon.intersection(second_polygon).area
    union_area = first_polygon.area + second_polygon.area - inter_area
    return inter_area / union_area


def jaccard_index(first_polygon: list, second_polygon: list):
    """
    Calculate Jaccard Index according to https://khurramjaved.com/RecursiveCNN.pdf
    Args:
        first_polygon: List of coordinates of a polygon or list of lists of coordinates
        second_polygon: List of coordinates of a second polygon or list of lists of coordinates
    Returns:
        float: Jaccard Index
    """
    # non batch case
    if len(np.array(first_polygon).shape) == 2 and len(np.array(second_polygon).shape) == 2:
        return _jaccard_index(first_polygon, second_polygon)
    # batch case
    all_results = []
    if len(np.array(first_polygon).shape) == 3 and len(np.array(second_polygon).shape) == 3:
        for first_polygon, second_polygon in zip(first_polygon, second_polygon):
            all_results.append(_jaccard_index(first_polygon, second_polygon))
    return np.mean(all_results)


def make_predictions(test_samples, model):
    all_coordinates = []
    all_images = []
    for sample_path in test_samples:
        sample = Sample(sample_path)
        all_coordinates.append(sample._get_coordinates(flatten=True))
        all_images.append(sample._get_image(shape=model.input_shape[1:3]))
        # make predictions
    all_predictions = model.predict(np.array(all_images))
    all_coordinates = np.array(all_coordinates)
    # all_predictions_tupled = np.reshape(all_predictions, (-1, 4, 2))
    return all_coordinates, all_predictions
    # TODO: remove these tupled lists...can just be done with reshape...


def calc_jaccard(all_coordinates, all_predictions, test_samples):

    all_jaccard = []
    all_coordinates_tupled = np.reshape(all_coordinates, (-1, 4, 2))
    all_predictions_tupled = np.reshape(all_predictions, (-1, 4, 2))
    for prediction, coordinates in zip(all_predictions_tupled, all_coordinates_tupled):
        all_jaccard.append(jaccard_index(coordinates, prediction))
    all_jaccard = np.array(all_jaccard)
    mean_jaccard = np.mean(all_jaccard)
    sorted_indices = all_jaccard.argsort()
    top_5_jaccard = defaultdict(dict)
    # TODO: why only top / flop 5?
    for index in sorted_indices[-5:]:
        top_5_jaccard[test_samples[index]
                      ]['jaccard_index'] = all_jaccard[index]
        top_5_jaccard[test_samples[index]
                      ]['prediction'] = all_predictions[index]
        # top_5_jaccard[test_samples[index]]['image'] = all_images[index]

    flop_5_jaccard = defaultdict(dict)
    for index in sorted_indices[:5]:
        flop_5_jaccard[test_samples[index]
                       ]['jaccard_index'] = all_jaccard[index]
        flop_5_jaccard[test_samples[index]
                       ]['prediction'] = all_predictions[index]
        # flop_5_jaccard[test_samples[index]]['image'] = all_images[index]

    ret_dict = {'mean': mean_jaccard,
                'top_5': top_5_jaccard, 'flop_5': flop_5_jaccard}

    return ret_dict


def calc_mae(all_coordinates, all_predictions, test_samples):
    all_errors = all_coordinates - all_predictions
    # make errors abs
    all_abs_errors = np.abs(all_errors)
    # calc mean
    mean_errors = np.mean(all_abs_errors, axis=1)
    mean_error = np.mean(mean_errors)
    sorted_indices = mean_errors.argsort()
    top_5 = sorted_indices[:5]
    flop_5 = sorted_indices[-5:]

    top_5_mae = defaultdict(dict)
    for index in sorted_indices[:5]:
        top_5_mae[test_samples[index]
                  ]['mae'] = mean_errors[index]
        top_5_mae[test_samples[index]
                  ]['prediction'] = all_predictions[index]
        # top_5_jaccard[test_samples[index]]['image'] = all_images[index]

    flop_5_mae = defaultdict(dict)
    for index in sorted_indices[-5:]:
        flop_5_mae[test_samples[index]
                   ]['mae'] = mean_errors[index]
        flop_5_mae[test_samples[index]
                   ]['prediction'] = all_predictions[index]
        # flop_5_jaccard[test_samples[index]]['image'] = all_images[index]

    ret_dict = {'mean': mean_error, 'top_5': top_5_mae, 'flop_5': flop_5_mae}

    return ret_dict


def jaccard_graphic(jaccard_results, out_path=None):
    fig, axs = plt.subplots(2, 5, figsize=(25, 25))
    fig.suptitle('Jaccard Index: Top 5(upper row) and Flop 5(lower row)')
    # top 5
    j = -1
    for sample_path, result in jaccard_results['top_5'].items():
        sample = Sample(sample_path)
        orig_height, orig_width = sample._get_image().shape[0:2]
        # assert orig_height, orig_width == all_images[index].shape[0:2]
        # translated_prediction = []
        prediction = result['prediction']

        # for i in range(0, 8, 2):
        #     translated_prediction.append(
        #         (prediction[i] * orig_width, prediction[i+1] * orig_height))
        # print(f'{translated_prediction=}')
        prediction = np.reshape(np.array(prediction), (4, 2))
        prediction[:, 0] = prediction[:, 0] * orig_width
        prediction[:, 1] = prediction[:, 1] * orig_height
        prediction = prediction.astype(int)
        ### write to image and show ###
        img = sample.visualize(style=('lines', ), data_type='corner_detection',
                               prediction=prediction, show=False)
        axs[0][j].set_title(
            f'{sample_path}: {result["jaccard_index"]:.2f}')
        axs[0][j].imshow(img)
        j -= 1
    # flop 5
    j = 0
    for sample_path, result in jaccard_results['flop_5'].items():
        sample = Sample(sample_path)
        orig_height, orig_width = sample._get_image().shape[0:2]
        # assert orig_height, orig_width == all_images[index].shape[0:2]
        # translated_prediction = []
        prediction = result['prediction']
        prediction = np.reshape(np.array(prediction), (4, 2))
        prediction[:, 0] = prediction[:, 0] * orig_width
        prediction[:, 1] = prediction[:, 1] * orig_height
        prediction = prediction.astype(int)
        # for i in range(0, 8, 2):
        #     translated_prediction.append(
        #         (prediction[i] * orig_width, prediction[i+1] * orig_height))
        ### write to image and show ###
        img = sample.visualize(style=('lines', ), data_type='corner_detection',
                               prediction=prediction, show=False)
        axs[1][j].set_title(
            f'{sample_path}: {result["jaccard_index"]:.2f}')
        axs[1][j].imshow(img)
        j += 1
    if out_path:
        jaccard_path = Path(out_path, 'JACCARD_top5_and_flop5.png')
        fig.savefig(jaccard_path)
    else:
        fig.show()


def mae_graphic(mae_results, out_path):
    fig, axs = plt.subplots(2, 5, figsize=(25, 25))
    fig.suptitle('Top 5(upper row) and Flop 5(lower row)')
    # top 5
    j = 0
    for sample_path, result in mae_results['top_5'].items():
        sample = Sample(sample_path)
        orig_height, orig_width = sample._get_image().shape[0:2]
        # assert orig_height, orig_width == all_images[index].shape[0:2]
        # translated_prediction = []
        prediction = result['prediction']
        prediction = np.reshape(np.array(prediction), (4, 2))
        prediction[:, 0] = prediction[:, 0] * orig_width
        prediction[:, 1] = prediction[:, 1] * orig_height
        prediction = prediction.astype(int)
        # for i in range(0, 8, 2):
        #     translated_prediction.append(
        #         (prediction[i] * orig_width, prediction[i+1] * orig_height))
        ### write to image and show ###
        img = sample.visualize(style=('lines', ), data_type='corner_detection',
                               prediction=prediction, show=False)
        axs[0][j].set_title(
            f'{sample_path}: {result["mae"]:.4f}')
        axs[0][j].imshow(img)
        j += 1
    # flop 5
    j = -1
    for sample_path, result in mae_results['flop_5'].items():
        sample = Sample(sample_path)
        orig_height, orig_width = sample._get_image().shape[0:2]
        # assert orig_height, orig_width == all_images[index].shape[0:2]
        # translated_prediction = []
        prediction = result['prediction']
        prediction = np.reshape(np.array(prediction), (4, 2))
        prediction[:, 0] = prediction[:, 0] * orig_width
        prediction[:, 1] = prediction[:, 1] * orig_height
        prediction = prediction.astype(int)
        # for i in range(0, 8, 2):
        #     translated_prediction.append(
        #         (prediction[i] * orig_width, prediction[i+1] * orig_height))
        ### write to image and show ###
        img = sample.visualize(style=('lines', ), data_type='corner_detection',
                               prediction=prediction, show=False)
        axs[1][j].set_title(
            f'{sample_path}: {result["mae"]:.4f}')
        axs[1][j].imshow(img)
        j -= 1
    if out_path:
        mae_path = Path(out_path, 'MAE_top5_and_flop5.png')
        fig.savefig(mae_path)
    else:
        fig.show()
