'''
Tests for calculating the jaccard index
'''

# System imports
import sys

# 3rd party imports
import pytest
import numpy as np

# local imports
from ....utils.eval_utils import jaccard_index, make_predictions, calc_jaccard
from ....data import Dataset, Sample
from paz.models.detection.paper_detection import PaperDetection

# end file header
__author__ = 'Adrian Lubitz'


def test_same_polygon():
    # clock-wise
    polygon = np.array([(0, 0), (1, 0), (1, 1), (0, 1)])
    assert jaccard_index(polygon, polygon) == pytest.approx(1)
    # counter clock-wise
    polygon = np.flip(polygon, 0)
    assert jaccard_index(polygon, polygon) == pytest.approx(1)


def test_positiv_smaller_one():
    """
    Jaccard index should always be positive and <= 1
    """
    d = Dataset()
    for sample_path in d.get_all_samples():
        sample = Sample(sample_path)
        coordinates = sample._get_coordinates(flatten=False)
        other_sample = Sample(d.get_random_sample())
        other_coordinates = other_sample._get_coordinates(flatten=False)
        jaccard = jaccard_index(coordinates, other_coordinates)
        # There is no >= and <= with pytest.approx therefore using epsilon
        # Rounding seems to be more than 1*epsilon -> therefore using 2*epsilon
        assert jaccard <= 1 + 2*sys.float_info.epsilon and jaccard >= 0 - sys.float_info.epsilon


def test_no_intersection():
    polygon = np.array([(0, 0), (1, 0), (1, 1), (0, 1)])
    other_polygon = polygon + 5
    assert jaccard_index(polygon, other_polygon) == pytest.approx(0)


def test_same_batch():
    polygons = [np.array([(0, 0), (1, 0), (1, 1), (0, 1)])] * 10
    assert jaccard_index(polygons, polygons) == pytest.approx(1)


def test_batch_no_intersection():
    polygons = [np.array([(0, 0), (1, 0), (1, 1), (0, 1)])] * 10
    other_polygons = [np.array([(0, 0), (1, 0), (1, 1), (0, 1)])+5] * 10
    assert jaccard_index(polygons, other_polygons) == pytest.approx(0)


def test_calc_jaccard():
    """
    Testing if calc_jaccard calculates the same results on multiple calls
    """
    d = Dataset()
    train_samples, valid_samples, test_samples = d.make_split()
    model = PaperDetection()

    all_coordinates, all_predictions = make_predictions(
        test_samples, model)
    jaccard_results = calc_jaccard(
        all_coordinates, all_predictions, test_samples)

    _all_coordinates, _all_predictions = make_predictions(
        test_samples, model)
    _jaccard_results = calc_jaccard(
        _all_coordinates, _all_predictions, test_samples)
    # Mean should be equal
    assert _jaccard_results['mean'] == jaccard_results['mean']
    # Top/Flop 5 pathes should be equal
    assert _jaccard_results['top_5'].keys(
    ) == jaccard_results['top_5'].keys()
    assert _jaccard_results['flop_5'].keys(
    ) == jaccard_results['flop_5'].keys()
    #  only using jaccard_index because prediction should not be tested here
    for path in jaccard_results['top_5']:
        assert _jaccard_results['top_5'][path]['jaccard_index'] == jaccard_results['top_5'][path]['jaccard_index']
    for path in jaccard_results['flop_5']:
        assert _jaccard_results['flop_5'][path]['jaccard_index'] == jaccard_results['flop_5'][path]['jaccard_index']
