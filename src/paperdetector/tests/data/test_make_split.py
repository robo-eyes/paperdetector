'''
Tests for making the split
'''

# System imports

# 3rd party imports
import pytest

# local imports
from ...data import Dataset

# end file header
__author__ = 'Adrian Lubitz'


def test_same_with_same_seed():
    for i in range(10):
        d = Dataset()
        d2 = Dataset()

        assert d.make_split(random_seed=i) == d2.make_split(random_seed=i)


def test_different_with_different_seed():
    for i in range(10):
        d = Dataset()
        d2 = Dataset()

        assert d.make_split(random_seed=i) != d2.make_split(random_seed=i+1)


def test_without_seed():
    for i in range(10):
        d = Dataset()
        d2 = Dataset()

        assert d.make_split() == d2.make_split()
