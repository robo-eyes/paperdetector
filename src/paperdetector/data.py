'''
helper for downloading and handling the dataset.
'''

# System imports
import multiprocessing
from pathlib import Path
import shutil
import random
import csv
from copy import deepcopy
from multiprocessing import Pool
import itertools


# 3rd party imports
from PIL import ImageDraw, ImageOps, Image
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from shapely.geometry import Polygon
from shapely.errors import TopologicalError
from paz.backend.image import resize_with_padding, resize_image
from paz.backend.keypoints import normalize_keypoints2D, denormalize_keypoints2D


# local imports
from .utils.download_utils import download_url

# end file header
__author__ = 'Adrian Lubitz'


class Dataset():
    """
    Class to handle dataset - this is an holds only the abstract describtion as path to image and csv files.
    File handling the data is done in the Sample class
    """

    def __init__(self) -> None:
        self.v3_url = "https://prod-dcd-datasets-cache-zipfiles.s3.eu-west-1.amazonaws.com/x3nm4cxr83-3.zip"
        self.v3_filename = "V3.zip"
        self.v3_foldername = "DATA_V3"
        self.v3_cleaned = 'DATA_V3_cleaned'
        # contains samples that are wrongly labeled
        self.clean_list = [37, 95, 1084, 44, 197, 1032, 1066, 1025, 126,
                           1081, 1089, 350, 371, 1031, 1087, 51, 36, 1083, 212, 329, 63, 1082, 177, 179, 147]

        self.download_data()

    def is_downloaded(self):
        """
        Check if file is already downloaded
        """
        return Path(self.v3_filename).is_file()

    def is_extracted(self):
        """
        Check if dataset is already extracted
        """
        return Path(self.v3_foldername).is_dir()

    def is_cleaned(self):
        """
        Check if dataset is already cleaned
        """
        return Path(self.v3_cleaned).is_dir()

    def extract(self):
        """
        Extract the dataset
        """
        shutil.unpack_archive(filename=self.v3_filename,
                              extract_dir=self.v3_foldername)

    def download_data(self):
        """
        Downloads and extracts the dataset from
        https://data.mendeley.com/datasets/x3nm4cxr83/2#folder-ea863ca1bb80-407b-91ac-aabea6a33ede
        """
        if self.is_cleaned():
            print('downloaded, extracted and cleaned')
            return
        if self.is_extracted():
            print('downloaded and extracted')
            self.clean()
            return
        if self.is_downloaded():
            print('downloaded but not extracted and cleaned')
            self.extract()
            self.clean()
            return
        print('starting download, extraction and cleaning')
        download_url(url=self.v3_url, output_path=self.v3_filename)
        self.extract()
        self.clean()

    def get_all_samples(self, cleaned=True):
        """
        returns a list of all the samples in the dataset
        Args:
            cleaned: If True the samples will come from the cleaned dataset
        Returns:
            list: a list of all the samples in the dataset(without jpg and csv extension)
        """
        if cleaned:
            directory = self.v3_cleaned
        else:
            directory = self.v3_foldername
        all_samples = Path(directory).glob('**/*')
        all_samples = [str(x.with_suffix(''))
                       for x in all_samples if x.is_file()]
        return sorted(list(set(all_samples)))

    def get_random_sample(self, **kwargs):
        """
        returns the path of a random sample(without jpg and csv extension)
        kwargs can be used to utilized the keyword args from get_all_samples
        Returns:
            str: path to a sample(without jpg and csv extension)
        """
        all_samples = self.get_all_samples(**kwargs)
        random_sample = random.choice(all_samples)
        random_sample = Path(random_sample).with_suffix('')
        return str(random_sample)

    def clean(self):
        """
        The dataset seems to be corrupted. This is a method to clean the dataset.
        """
        self.check_integrity(True)

    def check_integrity(self, clean=True, cleaned=False):
        """
        This checks the integrity of the dataset
        Args:
            clean: Flag if the data should be cleaned while checking the integrity
            cleaned: Flag to perform the check on the cleaned dataset
        """
        if clean:
            Path(self.v3_cleaned).mkdir(parents=True, exist_ok=True)
            corrupted_sample_list = []
            for wrong_sample in self.clean_list:
                corrupted_sample_list.append(f'({wrong_sample})')
        summary = []
        all_samples = self.get_all_samples(cleaned=cleaned)
        if len(all_samples) != 1111 and not cleaned:
            summary.append(
                f'The dataset should contain 1111 samples as stated in the paper but contains {len(all_samples)}')
        for sample in all_samples:
            sample = Path(sample)
            # TODO: perform geometric test
            if clean and sample.with_suffix('.jpg').is_file() and sample.with_suffix('.csv').is_file():
                sample_obj = Sample(sample)
                coordinates = sample_obj._get_coordinates(flatten=False)

                if not Polygon(coordinates).is_valid:
                    summary.append(
                        f'The coordinates for file {sample.with_suffix(".csv")} are incorrect')
                    corrupted_sample_list.append(sample.stem)

            if clean and sample.with_suffix('.jpg').is_file() and sample.with_suffix('.csv').is_file() and sample.stem not in corrupted_sample_list:
                shutil.copy(sample.with_suffix('.jpg'), self.v3_cleaned)
                shutil.copy(sample.with_suffix('.csv'), self.v3_cleaned)
            if not sample.with_suffix('.jpg').is_file():
                summary.append(
                    f'The image file {sample.with_suffix(".jpg")} is missing')
            if not sample.with_suffix('.csv').is_file():
                summary.append(
                    f'The csv file {sample.with_suffix(".csv")} is missing')
            if sample.with_suffix('.jpg 2').is_file():
                if sample.with_suffix('.jpg').is_file():
                    summary.append(
                        f"There is copy of the file {sample.with_suffix('.jpg')} with the name {sample.with_suffix('.jpg 2')}")
                else:
                    summary.append(
                        f"The file {sample.with_suffix('.jpg 2')} needs to be renamed to {sample.with_suffix('.jpg')}")
            if sample.with_suffix('.csv 2').is_file():
                if sample.with_suffix('.csv').is_file():
                    summary.append(
                        f"There is copy of the file {sample.with_suffix('.csv')} with the name {sample.with_suffix('.csv 2')}")
                else:
                    summary.append(
                        f"The file {sample.with_suffix('.csv 2')} needs to be renamed to {sample.with_suffix('.csv')}")

        if len(summary) != 0:
            prefix = 'original'
            if cleaned:
                prefix = 'cleaned'
            with open(f'{prefix}_integrity_check.log', 'w', encoding='utf-8') as report:
                print(
                    f'The dataset did not pass the integrity check. It has {len(summary)} errors.', file=report)
                for line in summary:
                    print(line, file=report)
            if not clean:
                raise Exception(
                    f'The dataset did not pass the integrity check. It has {len(summary)} errors. See {prefix}_integrity_check.log for further details')

    def make_split(self, random_seed=42, split_ratio=(0.8, 0.1, 0.1)):
        """
        Create the training, validation and test set.
        Args:
            random_seed: a random seed to make reproducable splits
            split_ratio: the ratio in which the set should be splitted.
        Returns:
            list:
                A list of lists containing the path to the files for training, validation and test set(without jpg and csv extension).
                [training_samples, validation_samples, test_samples]
        """
        assert sum(
            split_ratio) == 1, f"The sum of the split_ratio needs to be 1 but is {sum(split_ratio)}"
        random.seed(random_seed)
        all_samples = self.get_all_samples()
        random.shuffle(all_samples)
        num_all_samples = len(all_samples)
        train_index = int(num_all_samples * split_ratio[0])
        valid_index = int(num_all_samples * (split_ratio[0] + split_ratio[1]))
        train_samples = all_samples[:train_index]
        valid_samples = all_samples[train_index:valid_index]
        test_samples = all_samples[valid_index:]
        assert len(all_samples) == len(train_samples) + len(valid_samples) + \
            len(test_samples), "Split wasn't performed correctly!"
        return train_samples, valid_samples, test_samples


class Sample():
    """
    Class to handle data samples
    """
    # TODO: rotate all images to portrait mode. Attention: coordinates need to be rotated as well! - maybe this is not the best idea because it can introduce a bias - but could be tested...

    def __init__(self, path, data_type='corner_detection') -> None:
        """
        Initialize the sample
        Args:
            path: the path to the sample(without jpg and csv extension)
            data_type: Type of the data - either 'corner_detection' for the corner_detection model or 'corner_refiner' for the corner_refiner model
        """
        available_types = ['corner_detection', 'corner_refiner']
        if data_type not in available_types:
            raise ValueError(
                f'type must be one of {available_types} but is {data_type}')
        # self.data_type = data_type
        # if self.data_type == 'corner_detection':
        self.path = Path(path)
        csv_path = self.path.with_suffix('.csv')
        image_path = self.path.with_suffix('.jpg')
        self.img = tf.keras.preprocessing.image.load_img(image_path)
        self.img = ImageOps.exif_transpose(self.img)
        self.width, self.height = self.img.size
        # get coordinates
        # self.coordinates = []
        with open(csv_path, newline='', encoding='utf-8') as csv_file:
            reader = csv.reader(csv_file)
            data = np.array(list(reader)[0])[:8]  # always one line
        self.coordinates = data.reshape(4, 2).astype(np.int)
        # for i in range(0, 7, 2):  # TODO: this can be done explicitly without a loop - improves speed
        #     self.coordinates.append((int(data[i]), int(data[i+1])))

    def visualize(self, style=('dots', ), data_type='corner_detection', seperate=False, prediction=None, show=True):
        """
        Visualize the sample with the coordinates as dots or lines
        """
        colors = ('red', 'green', 'blue', 'purple')
        if data_type == 'corner_detection' and seperate is True:
            raise ValueError(
                "corner_detection images cannot be visualized seperately")
        posssible_styles = set(('dots', 'lines'))
        styles = set(style)
        intersection = styles & posssible_styles
        if not intersection:
            raise ValueError(
                f'style can only be one or a combination of{posssible_styles}')
        img = deepcopy(self.img)
        draw = ImageDraw.Draw(img)
        if 'dots' in style:
            # draw red dots
            for i, coordinate in enumerate(self.coordinates):
                draw.regular_polygon(
                    (coordinate, 50), n_sides=365, fill=colors[i])
        if data_type == 'corner_detection':
            if 'lines' in style:
                for i in range(4):
                    draw.line(
                        (tuple(self.coordinates[i]), tuple(self.coordinates[i-1])), fill=(0, 255, 0), width=10)
                if prediction is not None:
                    for i in range(4):
                        draw.line(
                            (tuple(prediction[i]), tuple(prediction[i-1])), fill=(255, 0, 0), width=10)

        if data_type == 'corner_refiner':
            if 'lines' in style:
                for i in range(4):
                    biggest_square = self._find_biggest_rect(
                        self.coordinates[i], [self.coordinates[i-1], self.coordinates[i-2], self.coordinates[i-3]])
                    for j in range(4):
                        # left, upper, right, and lower
                        if j % 2:
                            start = (biggest_square[j-3], biggest_square[j])
                            end = (biggest_square[j-1], biggest_square[j])
                        else:
                            start = (biggest_square[j], biggest_square[j-3])
                            end = (biggest_square[j], biggest_square[j-1])
                        draw.line(
                            (start, end), fill=colors[i], width=35-(i*4))
            if seperate:  # TODO: styles will be ignored in this case
                images, coordinates = self.get_data(
                    data_type='corner_refiner', absolut=True)
                f, axarr = plt.subplots(2, 2)
                pos = list(itertools.product([0, 1], repeat=2))
                i = 0
                for image, coordinate in zip(images, coordinates):
                    img = Image.fromarray(image.astype('uint8'), 'RGB')
                    draw = ImageDraw.Draw(img)
                    # TODO: something like this should as well be applied to the dots and lines in the other formats
                    dot_size = round(img.width * 0.02)
                    draw.regular_polygon(
                        (coordinate.tolist(), dot_size), n_sides=365, fill=(255, 0, 0))
                    axarr[pos[i][0], pos[i][1]].imshow(img)
                    i += 1
        if show:
            plt.title(str(self.path))  # TODO: this looks weird for seperate
            plt.imshow(img)
        return img

    def _get_corner_data(self, shape=None, absoulte=False, **kwargs):
        """
        Get the corner images with the respective coordinates
        Returns:
            list: a list containing X(the corner images) and Y(the corner coordinates in the new image) of the sample
        """
        images = []
        coordinates = []
        # This starts with TL corner and goes counter clockwise
        for i, point in enumerate(self.coordinates):
            # Find biggest possible square
            # there are always 4 coordinates, this will never be out of bounds
            left, upper, right, lower = self._find_biggest_rect(
                point, [self.coordinates[i-1], self.coordinates[i-2], self.coordinates[i-3]])
            # Use numpy slicing for cropping - PIL crop has a memory leak
            img = np.array(self.img)[upper:lower, left:right]
            height, width, dim = img.shape
            if height < 10 or width < 10 or width > self.img.width or height > self.img.height:
                # if the image is to small just ignore (too big should never happen)
                continue
            coordinate = (point[0] - left, point[1] - upper)
            rel_coordinate = (
                float(coordinate[0]) / width, float(coordinate[1]) / height)
            if shape is None:
                images.append(np.array(img))
                if absoulte:
                    coordinates.append(np.array(coordinate))
                else:
                    coordinates.append(np.array(rel_coordinate))
            else:
                images.append(np.array(resize_with_pad(
                    img, target_height=shape[1], target_width=shape[0], **kwargs)))
                width = images[-1].shape[1]
                height = images[-1].shape[1]
                if not absoulte:
                    coordinates.append(np.array(rel_coordinate))
                else:
                    abs_coordinate = (
                        round(rel_coordinate[0]*width), round(rel_coordinate[1]*height))
                    coordinates.append(np.array(abs_coordinate))
        return images, coordinates

    def _get_image(self, shape=None, padding=False, **kwargs):
        """
        Get the numpy array of the image
        Returns:
            ndarray: numpy array of the image(s)
        """
        if shape is None:
            return np.array(self.img)
        else:
            if padding:
                return resize_with_padding(np.array(self.img), (shape[0], shape[1]), **kwargs)
            else:
                return resize_image(np.array(self.img), (shape[0], shape[1]), **kwargs)

    # TODO: these helper functions should be static methods - this enables me to use them later in inference

        # TODO: squre is only helpfull on inference. For Datacreation I need biggest rect
    def _find_biggest_rect(self, point, border_points, squre=False):
        """
        finds the biggest rectangle in the image that contains point while border_points define the max/min of the cropped rectangle.
        Args:
            point: the point that should be in the cropped rectangle
            border_points: the surrounding coordinates that should not be in the square (list of three tuples). position 0 and 2 are next to the point while 1 is on the opposite. 

        Returns:
            tuple:  a 4-tuple defining the left, upper, right, and lower pixel coordinate.
        """
        rects = {}  # a dict holding the possible rects as values and the area of the resulting rect as key
        # opposite_border_point = border_points.pop(1)
        for i, border_point in enumerate(border_points):
            opposite_border_point = border_points[i-2]
            other_border_point = border_points[i-1]
            # X set for border_point
            if border_point[0] > point[0]:
                right = border_point[0]
                left = 0
            else:
                left = border_point[0]
                right = self.width
            if other_border_point[1] > point[1]:
                lower = other_border_point[1]
                upper = 0
            else:
                upper = other_border_point[1]
                lower = self.height

            if not self._point_in_rect(opposite_border_point, (left, upper, right, lower)):
                #     # (right - left) * (lower - upper) = area of the rect
                rects[(right - left) * (lower - upper)
                      ] = (left, upper, right, lower)

            # X set for other_border_point
            if other_border_point[0] > point[0]:
                right = other_border_point[0]
                left = 0
            else:
                left = other_border_point[0]
                right = self.width
            if border_point[1] > point[1]:
                lower = border_point[1]
                upper = 0
            else:
                upper = border_point[1]
                lower = self.height
            if not self._point_in_rect(opposite_border_point, (left, upper, right, lower)):
                # (right - left) * (lower - upper) = area of the rect
                rects[(right - left) * (lower - upper)
                      ] = (left, upper, right, lower)

        return rects[max(rects)]

    def _create_border_points_projections(self, border_points):
        """
        creates the projections of the borderpoints on the x and y axis (4 points for every border point)
        Args:
            border_points: the surrounding coordinates that should not be in the square
        Returns:
            list: a list of tuples containing points projected on the x and y axis
        """
        projected_points = []
        for border_point in border_points:
            projected_points.append((border_point[0], 0))
            projected_points.append((border_point[0], self.height))
            projected_points.append((0, border_point[1]))
            projected_points.append((self.width, border_point[1]))
        return projected_points

    def _point_in_rect(self, point, rect):  # TODO: is this working correctly?
        """
        calcultes if a given point is in a given rect. 
        On the border counts as inside.
        Args:
            point: The point which should be measured
            rect: The rect is defined by (left, upper, right, lower)
        """
        left, upper, right, lower = rect
        return bool(point[0] >= left and point[0] <= right and point[1] >= upper and point[1] <= lower)

    def _dist_to_image_border(self, point, rect=None):
        """
        calculates the minimal dist to the image border
        Args:
            point: the point which should be measured
            rect: a rect around the point - if not given self.image is used as the rect. The rect is defined by its width and height -> (width, height)
        Returns:
            int:  the minimal distance from the point to the border in pixels
        """

        if rect is None:
            width = self.width
            height = self.height
        else:
            width, height = rect
        # assert point in rect
        if point[0] < 0 or point[0] > width or point[1] < 0 or point[1] > height:
            raise ValueError(
                f'Point must be in the given rect. Point is {point} while the given rect is {(width, height)}')
        min_x_to_image_border = min(width - point[0], point[0])
        min_y_to_image_border = min(height - point[1], point[1])
        return min(min_x_to_image_border, min_y_to_image_border)

    def _get_coordinates(self, absolute=False, flatten=True):
        """
        Get the corner coordinates.
        Args:
            absolute: Flag to return coordinates in absolute pixel values. Default is False which means coordinates will be returned as relative values.
            flatten: Flag if the data should be flatten to a (8, 1) shape
        Returns:
            ndarray: Array of the coordinates in the shape (4, 2) or (8, 1) depending on flatten flag
        """
        if absolute:
            if flatten:
                return np.array(self.coordinates).flatten()
            else:
                return np.array(self.coordinates)
        else:
            rel_coordinates = normalize_keypoints2D(
                self.coordinates, self.height, self.width, norm_range=(0, 1))
            if flatten:
                return np.array(rel_coordinates).flatten()
            else:
                return np.array(rel_coordinates)

    def get_data(self, shape=None, data_type="corner_detection", absolut=False, flatten=True, padding=False, **kwargs):
        # TODO: absoulte and shape together is not possible right now...could do some projection with rounding - would not recommend.
        if data_type == "corner_detection":
            images = [self._get_image(shape, padding=padding, **kwargs)]
            # TODO: if reshape with padding is used the coordinates need to be translated as well
            coordinates = [self._get_coordinates(
                absolute=absolut, flatten=flatten)]
        if data_type == "corner_refiner":
            images, coordinates = self._get_corner_data(
                shape=shape, absoulte=absolut, flatten=flatten, **kwargs)
        return images, coordinates


class DataGenerator(tf.keras.utils.Sequence):
    """
    Generator for the Dataset
    """

    def __init__(self, samples, data_type='corner_detection', augemtation_processor=None, shape=(64, 128), batch_size=64, shuffle=True, seed=42, multiprocessing=False):
        """
        Initializes the DataGenerator
        Args:
            samples: a list samples in the dataset(without jpg and csv extension)
            data_type: Type of the data - either 'corner_detection' for the corner_detection model or 'corner_refiner' for the corner_refiner model
            shape: Inputshape of the model. Images will be reshaped to this size using resize_with_pad
            batch_size: size of the batches that the generator produces
            shuffle: flag that denotes if samples will be shuffeled after each epoch 
            multiprocessing: flag that denotes if multiprocessing is used internally in the Generator - this is experimental and there may be another solution to use this
            augmentation: list of augmentations that should be used. Some are just applicable for specific data_type. Available augmentations are ['refiner_random_crop']
        """
        # available_augmentations = ['refiner_random_crop']
        # # TODO: check if union is bigger than available_augmentations -> raise ValueError
        available_types = ['corner_detection', 'corner_refiner']
        if data_type not in available_types:
            raise ValueError(
                f'type must be one of {available_types} but is {data_type}')
        random.seed(seed)
        self.shuffle = shuffle
        self.data_type = data_type
        self.shape = shape
        self.batch_size = batch_size
        self.data_list = samples
        self.multiprocessing = multiprocessing
        self.augmentation_processor = augemtation_processor

    def __len__(self):
        """
        Denotes the number of batches per epoch
        Returns:
            int: number of batches that will be produced
        """
        return int(np.ceil(len(self.data_list) / self.batch_size))

    def __getitem__(self, index):
        # Multithreading advantages. no new process needs to be created. Should be enough for I/O
        # Multiprocessing: Advantage can use the full power of all the cores but has some Overhead with process creation
        # Advantages of multiprocessing will be more relevant because resizing takes so long not loaing the image - which is weird...
        # TODO: maybe multiprocessing in the Generator is not neccesary because tf has a parameter for multiprocessing - which probably wraps some multiprocessing around the generator
        """
        Generate one batch of data
        Returns:
            list: A list of inputs and outputs [X, Y] where input and output have a length of batch_size
        """
        # get list of paths for batch
        if index < self.__len__() - 1:
            batch_samples = self.data_list[index *
                                           self.batch_size:(index+1)*self.batch_size]
        else:
            batch_samples = self.data_list[index*self.batch_size:]
        # get Image and relative coordinates of the corners
        X = []
        Y = []

        if self.multiprocessing:
            with Pool() as pool:
                zipped = pool.map(self._create_sample, batch_samples)
        else:
            zipped = map(self._create_sample, batch_samples)

        # array = np.array(list(zip(*zipped)))
        for x, y in zipped:
            X.extend(x)
            Y.extend(y)
        return np.array(X), np.array(Y)

    def on_epoch_end(self):
        """Updates indexes after each epoch"""
        if self.shuffle == True:
            np.random.shuffle(self.data_list)

    def _create_sample(self, path):
        """creates a list containing one or up to four images depending on data_type and the corresponding list with the coordinates"""
        sample = Sample(path, data_type=self.data_type)  # image loading
        x, y = sample.get_data(
            shape=self.shape, data_type=self.data_type)  # reshaping
        # do augmentation
        if self.augmentation_processor:
            # TODO: this will just work for corner_detection
            for image, coordinates in zip(x, y):
                _x, _y = self.augmentation_processor(
                    image, coordinates.reshape(4, 2))
            # TODO: this works only for corner_detection
            return [_x], [_y.reshape(8)]
        else:
            return x, y
